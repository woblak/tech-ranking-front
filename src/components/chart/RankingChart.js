import React, { useState } from 'react';
import '../../App.css';
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CountriesFilter from './elems/CountriesFilter';
import TagsFilter from './elems/TagsFilter';
import Chart from './elems/Chart';

export default function RankingChart() {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <h1>Chart</h1>
            <Grid container spacing={2}>
                <Grid item xs />
                <Grid item xs={11}>
                    <Card className={classes.paper}>
                        <div>
                            <Grid container spacing={1}>
                                <Grid item xs={2}>
                                    <CountriesFilter />
                                </Grid>
                                <Grid item xs={8}>
                                    <Chart />
                                </Grid>
                                <Grid item xs={2}>
                                    <TagsFilter />
                                </Grid>
                            </Grid>
                        </div>
                    </Card>
                </Grid>
                <Grid item xs />
            </Grid>
        </div >
    )
}


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: "center",
        color: theme.palette.text.primary
    }
}));