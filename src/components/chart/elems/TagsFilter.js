import React, { useState } from 'react';
import '../../../App.css';
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";

export default function TagsFilter() {

    const classes = useStyles();
    const [state, setState] = React.useState({
      age: "",
      name: "hai"
    });
  
    const handleChange = (event) => {
      const name = event.target.name;
      setState({
        ...state,
        [name]: event.target.value
      });
    };

    return (
        <div>
            <FormControl className={classes.formControl} >
                <InputLabel htmlFor="name-native-error">Country</InputLabel>
                <NativeSelect
                    value={state.name}
                    onChange={handleChange}
                    name="name"
                    inputProps={{
                        id: "name-native-error"
                    }}
                >
                    <optgroup label="Europe">
                        <option value="hai">CZ</option>
                        <option value="hai">GB</option>
                        <option value="hai">PL</option>
                    </optgroup>
                    <optgroup label="Asia">
                        <option value="olivier">CY</option>
                        <option value="kevin">JP</option>
                    </optgroup>
                </NativeSelect>
                <FormHelperText>
                    ISO 3166-1</FormHelperText>
            </FormControl>
        </div>
    )
}


const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  }));