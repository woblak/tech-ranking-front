import React, { useState } from 'react';
import '../../App.css';
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";

export default function RankingChart() {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <h1>Table</h1>
            <Grid container spacing={3}>
                <Grid item xs />
                <Grid item xs={9}>
                    <Card className={classes.paper}>
                        <p>Welcome to the visualisation of technologies and programming languages popularity ranking based on job advertisements from all over the world.</p>
                        <p>The algorithm runs every week and collects job ads from 52 countries by searching defined 107 tags.</p>
                    </Card>
                </Grid>
                <Grid item xs />
            </Grid>
        </div>
    )
}


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: "center",
        color: theme.palette.text.primary
    }
}));