import React, { useState } from 'react';
import RankingDescription from './description/RankingDescription';
import RankingChart from './chart/RankingChart';
import RankingTable from './table/RankingTable';

export default function Project() {

    return (
        <div>
            <RankingDescription />

            <RankingChart />

            <RankingTable />
        </div>
    )

}